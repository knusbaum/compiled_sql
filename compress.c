#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "table.h"

int main(void) {

    struct table *table = open_table("./people");
    if(table == NULL) {
        printf("Failed to open table. Exiting.\n");
        return 1;
    }

    vacuum_table_percent(table, 100);
    clean_data_files(table);
    close_table(table);

}

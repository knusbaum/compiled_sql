#ifndef TABLE_H
#define TABLE_H

#include <stdio.h>

#define FILE_NAME_SIZE 15

#define FR_EMPTY 1

struct file_record {
    char name[FILE_NAME_SIZE];
    int flags;
};

struct table_header {
    int version;
    char table_name[100];
    unsigned long filecount;
    size_t datsize;
    size_t recordsize;
//    size_t last_insert_datafile;
    struct file_record files[];
};

struct data_file {
    char *fname;
    off_t mapsize;
    void *mapping;
};

struct table {
    struct table_header *header;
    int headerfd;
    off_t mapsize;
    char *directory;
    size_t insert_datafile_index; // index into files
    size_t insert_offset;          // record offset relative to insert_datafile
    struct data_file *insert_datafile;
    struct data_file *data_files;
};

#define VALID 1

struct row_header {
    unsigned char flags;
};

struct iterator {
    struct table *table;
    struct data_file *currdf;
    size_t index;
    size_t recordsize;
    size_t datsize;
    size_t curr_file_index;
    size_t curr_record_offset;
};

int create_table(char *tabledir, char *name, off_t datfile_size, size_t recordsize);
struct table *open_table(char *tabledir);
void close_table(struct table *table);

void fprint_table(FILE *restrict stream, struct table *table);

int add_file(struct table *table);

struct data_file *get_data_file(struct table *table, off_t i);

void *get_insert(struct table *table);

struct iterator *get_iterator(struct table *table);
void *get_record(struct iterator *iterator);

void vacuum_table(struct table *table);
void vacuum_table_percent(struct table *table, int percent);
void vacuum_data_file(struct table *table, size_t df_index);

void clean_data_files(struct table *table);

#endif

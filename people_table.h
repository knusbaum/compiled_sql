#ifndef PEOPLE_TABLE_H
#define PEOPLE_TABLE_H

#include <time.h>
#include "table.h"

struct people {
    struct row_header header;
    char fname[50];
    char lname[50];
    time_t birthday;
};

int create_people_table(char *tabledir, char *name);

struct people *get_row_i(struct table *table, off_t i);

#endif

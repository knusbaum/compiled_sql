#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "table.h"
#include "people_table.h"

int main(void) {

    struct table *table = open_table("./people");
    if(table == NULL) {
        printf("Failed to open table. Exiting.\n");
        return 1;
    }

    fprint_table(stdout, table);

    size_t records_per_file = table->header->datsize / table->header->recordsize;
    printf("Expecting %ld records per file.\n", records_per_file);
    for(int i = 0; i < table->header->filecount; i++) {
        struct data_file *df = get_data_file(table, i);
        if(df == NULL) {
            printf("Failed to get data file %d\n", i);
        }
        struct people *people = df->mapping;

        size_t live_records = 0, dead_records = 0;
        for(int j = 0; j < records_per_file; j++) {
            if(people[j].header.flags & VALID) {
                live_records++;
            }
            else {
                dead_records++;
            }
        }
        printf("%s: FLAGS: %d live: %ld dead: %ld percent: %lu\n",
               df->fname,
               table->header->files[i].flags,
               live_records,
               dead_records,
               (live_records * 100)/records_per_file);

    }

    //vacuum_table(table);

    close_table(table);
}

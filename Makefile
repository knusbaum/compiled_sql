CC = gcc
CFLAGS = -O2 -ggdb -Wall -Werror
LDFLAGS =
DB = db
STATS = db_stats
COMPRESS = db_compress
EXECUTABLES = $(DB) $(STATS) $(COMPRESS)


#CFILES=$(shell find . -name "*.c")
CFILES = people_table.c table.c
OBJECTS=$(patsubst %.c,%.o,$(CFILES))
ALLCFILES = $(CFILES) main.c stats.c
DEPFILES=$(patsubst %.c,./deps/%.d,$(ALLCFILES))

all : $(EXECUTABLES)

-include $(DEPFILES)

clean :
	rm -f *.o

nuke : clean
	rm -Rf ./deps *~ $(EXECUTABLES)

$(DB) : $(OBJECTS) main.o
	$(CC) $(CFLAGS) $(LDFLAGS) $(OBJECTS) main.o -o $(DB)

$(STATS) : $(OBJECTS) stats.o
	$(CC) $(CFLAGS) $(LDFLAGS) $(OBJECTS) stats.o -o $(STATS)

$(COMPRESS) : $(OBJECTS) compress.o
	$(CC) $(CFLAGS) $(LDFLAGS) $(OBJECTS) compress.o -o $(COMPRESS)

deps/%.d : %.c
	@mkdir -p deps
	@mkdir -p `dirname $@` 
	@echo -e "[MM]\t\t" $@
	@$(CC) $(CFLAGS) -MM $< -MF $@

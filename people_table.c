#include "people_table.h"

#define RECORDS_PER_FILE 100000

int create_people_table(char *tabledir, char *name) {
    int ok = create_table(tabledir, name, (sizeof (struct people) * RECORDS_PER_FILE), sizeof (struct people));
    if(!ok) {
        printf("Failed to create table. Exiting.\n");
        return 0;
    }
    return 1;
}

struct people *get_row_i(struct table *table, off_t i) {
    if(i > table->header->filecount * RECORDS_PER_FILE) {
        return NULL;
    }

    int dat_idx = i / RECORDS_PER_FILE;
    struct data_file *df = get_data_file(table, dat_idx);
    if(df == NULL) {
        printf("Failed to get data file.\n");
        return NULL;
    }
    struct people *data = df->mapping;
    off_t offset = i % RECORDS_PER_FILE;
    return data + offset;
}

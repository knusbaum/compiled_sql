#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include "table.h"

void set_next_insert_indexes(struct table *table);

static char *header_file(char *tabledir) {
    char *fname = "header.db";
    char *pathname = malloc(strlen(tabledir) + strlen(fname) + 2);
    strcpy(pathname, tabledir);
    strcat(pathname, "/");
    strcat(pathname, fname);
    return pathname;
}

static char *datafile_i(unsigned long id) {
    char *fname = malloc(FILE_NAME_SIZE);
    sprintf(fname, "%010lu.dat", id);
    return fname;
}

static char *data_file(const char *tabledir, char *fname) {
    char *pathname = malloc(strlen(tabledir) + strlen(fname) + 2);
    strcpy(pathname, tabledir);
    strcat(pathname, "/");
    strcat(pathname, fname);
    return pathname;
}

int create_table(char *tabledir, char *name, off_t datfile_size, size_t recordsize) {
    if(strlen(name) >= 100) {
        printf("Cannot create table with name more than 99 characters long.\n");
        return 0;
    }

    char *pathname = header_file(tabledir);
    int fd = open(pathname, (O_CREAT | O_TRUNC | O_RDWR), 0660);
    free(pathname);
    if(fd == -1) {
        perror("Failed to open table ");
        return 0;
    }

    struct table_header header;
    header.version = 1;
    memset(header.table_name, 0, sizeof header.table_name);
    strcpy(header.table_name, name);
    header.filecount = 0;
    header.datsize = datfile_size;
    header.recordsize = recordsize;
    //header.last_insert_datafile = 0;

    ftruncate(fd, sizeof header);
    fsync(fd);

    char *headermap = mmap(NULL, sizeof header,
                        (PROT_READ | PROT_WRITE), MAP_SHARED, fd, 0);
    if(headermap == MAP_FAILED) {
        perror("Failed to map table header ");
        return 0;
    }
    memcpy(headermap, &header, sizeof header);
    munmap(headermap, sizeof header);
    close(fd);
    return 1;
}

///**
// * This function is called when a table is opened.
// * its job is to set the insert datafile and offset
// * based on the last saved insert_datafile.
// * This is necessary to avoid sparsely populated data
// * files during small insert/shut-down cycles.
// */
//void bootup_scan_insert_datafile(struct table *table) {
//
//
//}

struct table *open_table(char *tabledir) {
    char *pathname = header_file(tabledir);
    int fd = open(pathname, O_RDWR);
    free(pathname);
    if(fd == -1) {
        perror("Failed to open table ");
        return NULL;
    }

    off_t sz = lseek(fd, 0, SEEK_END);
    lseek(fd, 0, SEEK_SET);

    struct table *table = malloc(sizeof (struct table));
    void *header = mmap(NULL, sz, (PROT_READ | PROT_WRITE), MAP_SHARED, fd, 0);
    if(header == MAP_FAILED) {
        perror("Failed to map table header ");
        return NULL;
    }

    table->header = header;
    table->headerfd = fd;
    table->mapsize = sz;
    table->directory = strdup(tabledir);
    size_t df_size = sizeof (struct data_file) * table->header->filecount;
    table->data_files = malloc(df_size);
    memset(table->data_files, 0, df_size);

    // Set up the next insert indexes.
    set_next_insert_indexes(table);

    return table;
}

void close_data_file(struct data_file *df) {
    if(df->fname == NULL) {
        return;
    }
    free(df->fname);
    munmap(df->mapping, df->mapsize);
    memset(df, 0, sizeof (struct data_file));
}

void close_table(struct table *table) {
    free(table->directory);
    for(int i = 0; i < table->header->filecount; i++) {
        close_data_file(table->data_files + i);
    }
    free(table->data_files);
    munmap(table->header, table->mapsize);
    close(table->headerfd);
    free(table);
}

int add_file(struct table *table) {
    // Expand the table header to hold the new file record.
    int ret = ftruncate(table->headerfd,
                        table->mapsize + sizeof (struct file_record));
    if(ret == -1) {
        perror("Failed to extend table header ");
        return 0;
    }
    fsync(table->headerfd);

    // Re-map the new table header.
    munmap(table->header, table->mapsize);
    void *new_map = mmap(table->header, table->mapsize + sizeof (struct file_record),
                         (PROT_READ | PROT_WRITE), MAP_SHARED, table->headerfd, 0);
    if(new_map == MAP_FAILED) {
        perror("Failed to remap table header ");
        return 0; // Maybe should abort here instead? Is table in consistent state?
    }
    table->header = new_map;
    table->mapsize += sizeof (struct file_record);

    // Create the data file
    char *datname = datafile_i(table->header->filecount);
    char *datfile = data_file(table->directory, datname);
    int fd = open(datfile, (O_CREAT | O_TRUNC | O_RDWR), 0660);
    free(datfile);
    if(fd == -1) {
        free(datname);
        perror("Failed to create new data file ");
        return 0;
    }
    // Expand new data file to expected size.
    ftruncate(fd, table->header->datsize);
    close(fd);

    // Write the file name and new file count to the table header.
    strcpy(table->header->files[table->header->filecount].name, datname);
    table->header->files[table->header->filecount].flags = FR_EMPTY;
    table->header->filecount++;
    msync(table->header, table->mapsize, MS_SYNC);
    free(datname);

    // Expand table's data_files array for new file.
    size_t df_size = sizeof (struct data_file) * table->header->filecount;
    table->data_files = realloc(table->data_files, df_size);
    memset(table->data_files + table->header->filecount-1, 0, sizeof (struct data_file));

    return 1;
}

struct data_file *get_data_file(struct table *table, off_t i) {
    //printf("Getting data file %lu\n", i);
    if(i >= table->header->filecount) {
        return NULL;
    }

    //printf("Checking data file cache.\n");
    if(table->data_files[i].fname != NULL) {
        //printf("data_files[%lu] fname: %p\n", i, table->data_files[i].fname);
        //printf("have [%s] in cache.\n", table->data_files[i].fname);
        return table->data_files + i;
    }

    //printf("Opening data file.\n");
    char *datfile = data_file(table->directory, table->header->files[i].name);
    //printf("Opening data file %s\n", datfile);
    int fd = open(datfile, O_RDWR);
    free(datfile);
    if(fd == -1) {
        printf("ERRNO: %d\n", errno);
        perror("Failed to open data file ");
        if(errno == EMFILE) {
            printf("Too many open files.\n");
            // Too many files open.
            for(long i = 0; i < table->header->filecount; i++) {
                //printf("Trying to close data file[%d]\n", i);
                close_data_file(&table->data_files[i]);
            }
            return get_data_file(table, i);
        }
        return NULL;
    }

    struct data_file *df = table->data_files + i;
    df->fname = strdup(table->header->files[i].name);
    df->mapsize = table->header->datsize;
    //printf("Mapping data file [%ld]\n", i);
    df->mapping = mmap(NULL, table->header->datsize, (PROT_READ | PROT_WRITE), MAP_SHARED, fd, 0);
    close(fd);
    if(df->mapping == MAP_FAILED) {
        perror("Failed to map data file ");
        return NULL;
    }
    return df;
}

void set_next_insert_indexes(struct table *table) {
    for(int i = 0; i < table->header->filecount; i++) {
        if(table->header->files[i].flags & FR_EMPTY) {
            //table->header->last_insert_datafile = i;
            table->insert_datafile_index = i;
            table->insert_offset = 0;
            table->insert_datafile = get_data_file(table, i);
            return;
        }
    }

    // If we didn't find an empty record, add a new file.
    int ok = add_file(table);
    if(!ok) {
        //table->header->last_insert_datafile = 0;
        table->insert_datafile_index = 0;
        table->insert_offset = 0;
        table->insert_datafile = NULL;
        return;
    }

    size_t insert_df_index = table->header->filecount-1;
    //table->header->last_insert_datafile = i;
    table->insert_datafile_index = insert_df_index;
    table->insert_offset = 0;
    table->insert_datafile = get_data_file(table, insert_df_index);

    //msync(table->header, table->mapsize);
}

void *get_insert(struct table *table) {
    if(table->insert_offset >= table->header->datsize) {
        // We're out of room in this data file.
        // Get another one.
        set_next_insert_indexes(table);
    }
    if(!table->insert_datafile) {
        return NULL;
    }
    table->header->files[table->insert_datafile_index].flags &= ~FR_EMPTY;
    void *ret = table->insert_datafile->mapping + table->insert_offset;
    table->insert_offset += table->header->recordsize;
    return ret;
}

void fprint_table(FILE *restrict stream, struct table *table) {
    fprintf(stream,
            "Table [%s]\n\theader: %p\n\t\tversion: %d\n\t\tfilecount: %lu\n\tmapsize: %lu\n\tdirectory: %s\n",
            table->header->table_name,
            table->header,
            table->header->version,
            table->header->filecount,
            table->mapsize,
            table->directory);
}

struct iterator *get_iterator(struct table *table) {
    struct iterator *it = malloc(sizeof (struct iterator));
    it->table = table;
    it->currdf = get_data_file(table, 0);
    it->index = 0;
    it->recordsize = table->header->recordsize;
    it->datsize = table->header->datsize;
    it->curr_file_index = 0;
    it->curr_record_offset = 0;
    return it;
}

void *get_record(struct iterator *iterator) {
    while (1) {
        if(iterator->curr_record_offset >= iterator->datsize) {
            do {
                iterator->curr_file_index++;
            } while (iterator->table->header->files[iterator->curr_file_index].flags & FR_EMPTY);
            // Need to get next data file.
            iterator->currdf = get_data_file(iterator->table, iterator->curr_file_index);
            // Prefetch next data file?
            //get_data_file(iterator->table, iterator->curr_file_index+1);
            if(!iterator->currdf) {
                return NULL;
            }
            //madvise(iterator->currdf->mapping, iterator->currdf->mapsize, MADV_SEQUENTIAL);
            iterator->curr_record_offset = 0;
        }

        struct row_header *hdr = iterator->currdf->mapping + iterator->curr_record_offset;

        iterator->curr_record_offset += iterator->recordsize;
        if(hdr->flags & VALID) {
            return hdr;
        }
    }
}

void vacuum_data_file(struct table *table, size_t df_index) {
    size_t records_per_file = table->header->datsize / table->header->recordsize;
    size_t offset = 0;

    for(int j = 0; j < records_per_file; j++) {
        // Need to get df repeatedly, since insert can realloc
        // the data_file array, or close the data_file;
        struct data_file *df = get_data_file(table, df_index);
        char *rows = df->mapping;
        struct row_header *header = (struct row_header *)rows + offset;
        //printf("Checking header [%d]\n", j);
        if(header->flags & VALID) {
//            printf("Copying record %d from %s to %s\n",
//                   j, df->fname, table->insert_datafile->fname);
            char *insert = get_insert(table);
            if(!insert) {
                printf("Failed to get a row to insert on!");
                // Just return. This file may have been partially vacuumed.
                // The db should still be consistent.
                return;
            }

            // re-get the source data file, in case getting the insert
            // moved or closed it. (this is ugly and should be changed)
            df = get_data_file(table, df_index);
            rows = df->mapping;
            memcpy(insert, rows + offset, table->header->recordsize);
            header->flags &= ~VALID;
        }
        offset += table->header->recordsize;
    }
    table->header->files[df_index].flags |= FR_EMPTY;
}

void vacuum_table(struct table *table) {
    vacuum_table_percent(table, 50);
}

void vacuum_table_percent(struct table *table, int percent) {
    size_t records_per_file = table->header->datsize / table->header->recordsize;
    printf("Expecting %ld records per file.\n", records_per_file);
    for(int i = 0; i < table->header->filecount; i++) {
        if(table->header->files[i].flags & FR_EMPTY) {
            // Skip empty data files.
            continue;
        }

        struct data_file *df = get_data_file(table, i);
        if(df == table->insert_datafile) {
            // Don't vacuum the current insert file.
            printf("Skipping the insert file [%s]\n", df->fname);
            continue;
        }
        if(df == NULL) {
            printf("Failed to get data file %d\n", i);
        }
        char *rows = df->mapping;
        size_t live_records = 0, dead_records = 0;
        for(int j = 0; j < records_per_file; j++) {
            struct row_header *header = (struct row_header *)rows;
            if(header->flags & VALID) {
                live_records++;
            }
            else {
                dead_records++;
            }
            rows += table->header->recordsize;
            int percent_full = (live_records * 100)/records_per_file;
            if(percent_full > percent) {
                break;
            }
        }
        int percent_full = (live_records * 100)/records_per_file;
        // If the data file is less than 10% full, vacuum it.
        if(percent_full <= percent) {
            printf("Vacuuming %s\n", df->fname);
            vacuum_data_file(table, i);
        }
    }
}

void clean_data_files(struct table *table) {
    size_t old_mapsize = table->mapsize;
    for(int i = table->header->filecount-1; i > 0; i--) {
        //printf("Checking file %d\n", i);
        if(table->header->files[i].flags & FR_EMPTY) {
            printf("Deleting file %d\n", i); 
            // close the data file if we have it mapped
            struct data_file *df = table->data_files + i;
            close_data_file(df);

            // Delete the data file
            char *datname = datafile_i(i);
            char *datfile = data_file(table->directory, datname);
            remove(datfile);
            free(datname);
            free(datfile);

            // decrease filecount.
            table->header->filecount--;
            // decrease the mapsize
            table->mapsize -= sizeof (struct file_record);
        }
        else {
            // Only delete files off the end.
            break;
        }
    }

    // Now that we may have deleted some data files, adjust the table header.
    if(old_mapsize != table->mapsize) {
        int ret = ftruncate(table->headerfd, table->mapsize);
        if(ret == -1) {
            perror("Failed to shrink table header ");
            // continue. It's OK if the file is long.
        }
        fsync(table->headerfd);
        munmap(table->header, old_mapsize);
        void *new_map = mmap(table->header, table->mapsize,
                             (PROT_READ | PROT_WRITE),
                             MAP_SHARED,
                             table->headerfd,
                             0);
        if(new_map == MAP_FAILED) {
            perror("Failed to remap table header ");
            return; // Maybe should abort here instead? Is table in consistent state?
        }
        table->header = new_map;
    }
}
